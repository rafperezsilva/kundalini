package com.keylimetie.swinggame

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.SharedPreferences
import android.content.pm.ActivityInfo
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.os.PersistableBundle
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.snackbar.Snackbar

open class BaseActivity : AppCompatActivity() {
    var sharedPreferences: SharedPreferences? = null
    override fun onCreate(savedInstanceState: Bundle?, persistentState: PersistableBundle?) {
        super.onCreate(savedInstanceState, persistentState)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        this.window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )

    }
}

/** HIDE KEYBOARD **/
fun BaseActivity.hideKeyboard() {
    try {
        val imm: InputMethodManager =
            getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(window.decorView.rootView.windowToken, 0)
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
    } catch (e: Exception) {
        Log.e("HIDE KEYBOARD ERROR", "${e.message}")
    }
}

fun BaseActivity.showMessage(msg: String) {
    hideKeyboard()
    try {
        val container = this.findViewById<View>(android.R.id.content)
        if (container != null) {
            Snackbar.make(container, msg, Snackbar.LENGTH_LONG).show()
        }
    } catch (e: Exception) {
        Log.e("ERROR", "ON DISPLAY MESSAGE ${e.message}")
    }
}

fun BaseActivity.showDialog(
    activity: AppCompatActivity,
    message: String,
    okListener: DialogInterface.OnClickListener
) {
    Handler(Looper.getMainLooper()).post {
        AlertDialog.Builder(activity)
            .setMessage(message)
            .setPositiveButton("OK", okListener)
            .setNegativeButton("Cancel", okListener)
            .create()
            .show()
    }
}