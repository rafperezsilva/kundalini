package com.keylimetie.swinggame

import android.annotation.SuppressLint
import android.content.DialogInterface
import android.graphics.Color
import android.media.MediaPlayer
import android.os.Bundle
import android.os.SystemClock
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.widget.*
import androidx.core.widget.addTextChangedListener
import com.keylimetie.swinggame.utils.PreferenceTypeKey
import com.keylimetie.swinggame.utils.Preferences
import java.util.*
import kotlin.collections.ArrayList


class MainActivity : BaseActivity(), View.OnClickListener, AdapterView.OnItemSelectedListener {
    var currentAction = Action()

    // var players =  ArrayList<Player>()
    val optionsSppiner = arrayListOf<String>("HOMBRE", "MUJER", "AMBOS")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        findViewById<Button>(R.id.add_player_mention).setOnClickListener(this)
        findViewById<Button>(R.id.add_dare_button).setOnClickListener(this)
        findViewById<Button>(R.id.add_players).setOnClickListener(this)
        findViewById<Button>(R.id.add_dares).setOnClickListener(this)
        findViewById<Button>(R.id.play).setOnClickListener(this)
        findViewById<Button>(R.id.start_timmer_button).setOnClickListener(this)
        findViewById<Button>(R.id.add_player_button).setOnClickListener(this)
        findViewById<Button>(R.id.next_action).setOnClickListener {
            try {
                mockData()
            } catch (e: Exception) {
                showMessage("${e.message}, borra todos los retos y asegurate de crear los jugadores y retos de manera correcta")
            }
        }
    }


    fun getPlayers(): ArrayList<Player> {
        return Preferences.getPlayersStored(this)?.players ?: ArrayList<Player>()
    }

    fun getActionsList(): ArrayList<Action> {
        return Preferences.getActionListStored(this)?.actions ?: ArrayList<Action>()
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        return if (keyCode == KeyEvent.KEYCODE_BACK) {
            findViewById<View>(R.id.play_game_view).visibility = View.GONE
            findViewById<View>(R.id.add_player_view).visibility = View.GONE
            findViewById<View>(R.id.add_dare_view).visibility = View.GONE
            findViewById<LinearLayout>(R.id.main_setting_view).visibility = View.VISIBLE
            findViewById<EditText>(R.id.name_ET).setText("")
            findViewById<Switch>(R.id.gender_switch).isChecked = false
            findViewById<Switch>(R.id.orientation_switch).isChecked = false
            true
        } else super.onKeyDown(keyCode, event)
    }

    fun mockData() {
        val actionSelected = if (getActionsList().size == 1) {
            getActionsList().first()
        } else {
            getActionsList()[Random().nextInt(0..getActionsList().size)]
        }
        findViewById<Chronometer>(R.id.chronometer).stop()
        currentAction = actionSelected
        val playersToMakeAction = when (actionSelected.canDoIt) {
            Gender.MALE -> {
                getPlayers().filter { it.gender == Gender.MALE } as ArrayList<Player>
            }
            Gender.FEMALE -> {
                getPlayers().filter { it.gender == Gender.FEMALE } as ArrayList<Player>
            }
            else -> {
                getPlayers()
            }
        }
        val playersToReceiveAction = when (actionSelected.onWho) {
            Gender.MALE -> {
                getPlayers().filter { it.gender == Gender.MALE } as ArrayList<Player>
            }
            Gender.FEMALE -> {
                getPlayers().filter { it.gender == Gender.FEMALE } as ArrayList<Player>
            }
            else -> {
                getPlayers()
            }
        }

        val playerActionSelected = if (playersToMakeAction.size <= 1) {
            playersToMakeAction.first()
        } else {
            playersToMakeAction[Random().nextInt(0..playersToMakeAction.size)]
        }
        if (playersToReceiveAction.contains(playerActionSelected)) {
            playersToReceiveAction.remove(playerActionSelected)
        }
        val playerReceiveSelected = when (playerActionSelected.orientation) {

            SexualOrientation.HETEROSEXUAL -> {
                val playersReceivers = playersToReceiveAction.filter {
                    when (playerActionSelected.gender) {
                        Gender.MALE -> {
                            it.gender == Gender.FEMALE
                        }
                        Gender.FEMALE -> {
                            it.gender == Gender.MALE
                        }
                        else -> it.gender == Gender.BOTH
                    }
                }
                try {
                    if (playersReceivers.size == 1) {
                        playersReceivers.first()
                    } else {
                        playersReceivers[Random().nextInt(0..playersReceivers.size)]
                    }
                } catch (e: Exception) {
                    Log.e("ERRO", "${e.message}")
                    Player()
                }

            }
            else -> {
                try {
                    if (playersToReceiveAction.size > 1) {
                        playersToReceiveAction[Random().nextInt(0..playersToReceiveAction.size)]
                    } else {
                        playersToReceiveAction.first()
                    }
                } catch (e: Exception) {
                    Log.e("ERRO", "${e.message}")
                    Player()
                }

            }

        }

        try {


            Log.i(
                "GAME ",
                "🦠 ${playerActionSelected.name} -> ${
                    actionSelected.action?.replace(
                        "{tripu}",
                        " ${playerReceiveSelected.name ?: ""} "
                    )
                }"
            )

            findViewById<TextView>(R.id.player_selected_TV).text = playerActionSelected.name
            findViewById<TextView>(R.id.dare_selected_TV).text = actionSelected.action?.replace(
                "{tripu}",
                "${playerReceiveSelected.name ?: ""}"
            )
        } catch (
            e: Exception
        ) {
            Log.e("ERRO", "${e.message}")
        }
        timmerChronoSetting(actionSelected)

    }

    fun timmerChronoSetting(actionSelected: Action) {
        val isTimeredTask = !(actionSelected.timeCount.toString()
            .isNullOrEmpty() || actionSelected.timeCount.toString() == "null")
        findViewById<LinearLayout>(R.id.timmer_linear_layout).visibility =
            if (!isTimeredTask) View.GONE else View.VISIBLE
        if (isTimeredTask) {
            findViewById<Chronometer>(R.id.chronometer).text =
                if (actionSelected.timeStandar == TimeStandard.MINUTES) {
                    "${actionSelected.timeCount.toString()}:00"
                } else {
                    "00:${actionSelected.timeCount.toString()}"
                }
        }
    }

    var newPLayer = Player()
    var newAction = Action()
    fun initAddPlayer() {

        findViewById<View>(R.id.add_player_view).visibility = View.VISIBLE
        findViewById<LinearLayout>(R.id.main_setting_view).visibility = View.GONE
        setSwitchValues()
        reloadPLayerList()
    }

    private fun initPlayGame() {
        if (getPlayers().size <= 1) {
            showMessage("Agrega por lo menos dos jugadores")
            return
        }
        if (getActionsList().size == 0) {
            showMessage("Agrega por lo menos un reto")
            return
        }
        findViewById<View>(R.id.play_game_view).visibility = View.VISIBLE
        findViewById<LinearLayout>(R.id.main_setting_view).visibility = View.GONE

        try {
            mockData()
        } catch (e: Exception) {
            showMessage("${e.message}, borra todos los retos y asegurate de crear los jugadores y retos de manera correcta")
        }
    }


    fun reloadPLayerList() {
        var playerNames = ArrayList<String>()
        val storedPlayers = Preferences.getPlayersStored(this)?.players
        storedPlayers?.forEach {
            playerNames.add(
                "${
                    if (it.gender == Gender.MALE) {
                        " 👱‍ "
                    } else {
                        " 👩 "
                    }
                } ${it.name ?: ""}"
            )
        }
        val lista = (findViewById(R.id.player_list) as? ListView)
        lista?.adapter = ArrayAdapter(
            this,
            R.layout.simple_custom_list,
            playerNames
        )
        lista?.setOnItemClickListener { parent, view, position, id ->
            Log.e("BORRAR", storedPlayers?.get(position)?.name ?: "")
            showDialog(
                this,
                "Quieres remover a ${storedPlayers?.get(position)?.name}?"
            ) { dialog, which ->
                when (which) {
                    DialogInterface.BUTTON_POSITIVE -> {
                        storedPlayers?.removeAt(position)
                        Preferences.saveDataPreference(
                            this,
                            PreferenceTypeKey.PLAYERS,
                            PlayerList(storedPlayers).toJson()
                        )
                        reloadPLayerList()
                    }
                    DialogInterface.BUTTON_NEGATIVE -> {
                        dialog.dismiss()
                    }
                }
            }


        }
    }

    fun dismisAddPlayer() {
        savePlayer()
        findViewById<View>(R.id.add_player_view).visibility = View.GONE
        findViewById<LinearLayout>(R.id.main_setting_view).visibility = View.VISIBLE
    }

    fun saveAction() {
        val actionStored = Preferences.getActionListStored(this)?.actions ?: ArrayList()
        actionStored.add(newAction)
        Preferences.saveDataPreference(
            this,
            PreferenceTypeKey.ACTIONS,
            ActionList(actionStored).toJson()
        )
        showMessage("Reto guardado")
        getActionsList()
        reloadDaresList()
    }

    fun dismisAddDare() {
        saveAction()
        findViewById<View>(R.id.add_dare_view).visibility = View.GONE
        findViewById<LinearLayout>(R.id.main_setting_view).visibility = View.VISIBLE
    }

    fun savePlayer() {
        val name = findViewById<EditText>(R.id.name_ET).text.toString()
        if (name.isNullOrEmpty()) {
            return
        }
        newPLayer.name = name
        val playersCurrent = Preferences.getPlayersStored(this)?.players ?: ArrayList()
        playersCurrent.add(newPLayer)
        Preferences.saveDataPreference(
            this,
            PreferenceTypeKey.PLAYERS,
            PlayerList(playersCurrent).toJson()
        )
        newPLayer = Player()
        findViewById<EditText>(R.id.name_ET).setText("")
        findViewById<Switch>(R.id.gender_switch).isChecked = false
        findViewById<Switch>(R.id.orientation_switch).isChecked = false
        getPlayers()

    }


    @SuppressLint("UseSwitchCompatOrMaterialCode")
    fun setSwitchValues() {
        val genderSwitch = findViewById<Switch>(R.id.gender_switch)
        val orientationSwitch = findViewById<Switch>(R.id.orientation_switch)

        orientationSwitch.text = if (newPLayer.orientation == SexualOrientation.HETEROSEXUAL) {
            SexualOrientation.HETEROSEXUAL.name
        } else {
            SexualOrientation.BISEXUAL.name
        }
        orientationSwitch.highlightColor =
            if (newPLayer.orientation == SexualOrientation.BISEXUAL) {
                R.color.pink
            } else {
                R.color.blue
            }
        genderSwitch.text = if (newPLayer.gender == Gender.MALE) {
            Gender.MALE.name
        } else {
            Gender.FEMALE.name
        }
        genderSwitch.highlightColor = if (newPLayer.gender == Gender.MALE) {
            Color.MAGENTA
        } else {
            Color.BLUE
        }
        orientationSwitch.setOnCheckedChangeListener { buttonView, isChecked ->
            hideKeyboard()
            newPLayer.orientation = if (isChecked) {
                SexualOrientation.HETEROSEXUAL
            } else {
                SexualOrientation.BISEXUAL
            }
            setSwitchValues()
        }
        genderSwitch.setOnCheckedChangeListener { buttonView, isChecked ->
            hideKeyboard()
            newPLayer.gender = if (isChecked) {
                Gender.MALE
            } else {
                Gender.FEMALE
            }
            setSwitchValues()
        }

    }

    private fun initDareView() {
        findViewById<View>(R.id.add_dare_view).visibility = View.VISIBLE
        findViewById<LinearLayout>(R.id.main_setting_view).visibility = View.GONE
        setDaresValues()
        reloadDaresList()
    }

    private fun setDaresValues() {
        val timmerStandardSwitch = findViewById<Switch>(R.id.switch_timmer_standart)
        val canDoItSpinner = findViewById<Spinner>(R.id.spinner_can_do_it)
        val onWhoSpinner = findViewById<Spinner>(R.id.spinner_on_who)
        val array_adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, optionsSppiner)
        array_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        canDoItSpinner.adapter = array_adapter
        canDoItSpinner.onItemSelectedListener = this
        onWhoSpinner.onItemSelectedListener = this
        onWhoSpinner.adapter = array_adapter
        timmerStandardSwitch.setOnCheckedChangeListener { buttonView, isChecked ->
            timmerStandardSwitch.text = if (isChecked) "SEGUNDOS" else "MINUTOS"
        }

    }

    private fun reloadDaresList() {
        var actions = ArrayList<String>()
        val storedActions = Preferences.getActionListStored(this)?.actions
        storedActions?.forEach { actions.add(it.action ?: "") }
        val lista = (findViewById(R.id.actions_list) as? ListView)
        lista?.adapter = ArrayAdapter(
            this,
            R.layout.simple_custom_list,
            actions
        )
        lista?.setOnItemClickListener { parent, view, position, id ->
            Log.e("BORRAR", storedActions?.get(position)?.action ?: "")
            showDialog(
                this,
                "Quieres remover este reto?"
            ) { dialog, which ->
                when (which) {
                    DialogInterface.BUTTON_POSITIVE -> {
                        storedActions?.removeAt(position)
                        Preferences.saveDataPreference(
                            this,
                            PreferenceTypeKey.ACTIONS,
                            ActionList(storedActions).toJson()
                        )
                        reloadDaresList()
                    }
                    DialogInterface.BUTTON_NEGATIVE -> {
                        dialog.dismiss()
                    }
                }
            }


        }
    }

    override fun onClick(v: View?) {
        hideKeyboard()
        when (v?.id) {
            R.id.start_timmer_button -> {
                val mediaPlayer = MediaPlayer.create(this, R.raw.beep)
                var isWorking = false
                val meter = findViewById<Chronometer>(R.id.chronometer)
                var timeElapse = SystemClock.elapsedRealtime()
                val timeDuration = (currentAction.timeCount
                    ?: 0) * if (currentAction.timeStandar == TimeStandard.MINUTES) 60000 else 1000
                meter.base = timeElapse + timeDuration
                meter.setOnChronometerTickListener {
                    val currentLayer = findViewById<View>(R.id.play_game_view)
                    when (it.text.toString()) {
                        "00:05" -> {
                            currentLayer.setBackgroundColor(Color.parseColor("#FF6200EE"))
                            mediaPlayer.start()
                        }
                        "00:04" -> {
                            currentLayer.setBackgroundColor(Color.RED)
                            mediaPlayer.start()
                        }
                        "00:03" -> {
                            currentLayer.setBackgroundColor(Color.CYAN)
                            mediaPlayer.start()
                        }
                        "00:02" -> {
                            currentLayer.setBackgroundColor(Color.GREEN)
                            mediaPlayer.start()
                        }

                        "00:01" -> {
                            currentLayer.setBackgroundColor(Color.BLUE)
                            mediaPlayer.start()
                        }

                        "00:00" -> {
                            val mediaPlayer2 = MediaPlayer.create(this, R.raw.orgasm)
                            mediaPlayer2.start()
                            meter.stop()
                            isWorking = false
                            currentLayer.setBackgroundColor(Color.BLACK)
                            timmerChronoSetting(currentAction)
                        }
                    }


                }
                if (!isWorking) {
                    isWorking = true
                    meter.start()

                } else {
                    meter.stop()
                    isWorking = false


                    findViewById<Button>(R.id.start_timmer_button).text =
                        (if (isWorking) "START" else "STOP")

                    Toast.makeText(
                        this@MainActivity,
                        if (isWorking) {
                            "R.string.working"
                        } else {
                            "R.string.stopped)"
                        },
                        Toast.LENGTH_SHORT
                    ).show()
                }

            }
            R.id.add_dare_button -> {
                val dareTV = findViewById<EditText>(R.id.dare_ET)
                val timeTV = findViewById<EditText>(R.id.time_duration_TV)
                if (dareTV.text.contains("{tripu}")) {
                    newAction.action = dareTV.text.toString()
                    if (!timeTV.text.toString().isNullOrEmpty()) {
                        try {
                            newAction.timeCount = timeTV.text.toString().toInt()
                            newAction.timeStandar =
                                if (findViewById<Switch>(R.id.switch_timmer_standart).isChecked) TimeStandard.SECONDS else TimeStandard.MINUTES
                        } catch (e: Exception) {
                            Log.e("ERROR CAST INT", "${e.message}")
                        }
                    }
                    dismisAddDare()
                    findViewById<EditText>(R.id.dare_ET).setText("")
                    timeTV.setText("")
                    findViewById<Switch>(R.id.switch_timmer_standart).isChecked = false
                } else {
                    showMessage("Debe mencionar el jugador")
                }


            }
            R.id.add_player_mention -> {
                val dareTV = findViewById<EditText>(R.id.dare_ET)
                dareTV.setText("${dareTV.text.toString()} {tripu} ")
            }
            R.id.add_players -> {
                initAddPlayer()
            }
            R.id.add_player_button -> {
                dismisAddPlayer()
            }
            R.id.play -> {
                initPlayGame()
            }
            R.id.add_dares -> {
                initDareView()
            }
        }
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        hideKeyboard()
        val actionPlayerType = when {
            optionsSppiner.get(position).equals("HOMBRE") -> {
                Gender.MALE
            }
            optionsSppiner.get(position).equals("MUJER") -> {
                Gender.FEMALE
            }
            else -> Gender.BOTH
        }
        when (parent?.id) {
            R.id.spinner_on_who -> {
                newAction.onWho = actionPlayerType
            }
            R.id.spinner_can_do_it -> {
                newAction.canDoIt = actionPlayerType
            }
        }
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
        hideKeyboard()
    }
}

fun Random.nextInt(range: IntRange): Int {
    return range.start + nextInt(range.last - range.start)
}
