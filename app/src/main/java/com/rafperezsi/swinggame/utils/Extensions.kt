@file:Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")

package com.keylimetie.playspinwin.utils

import android.annotation.SuppressLint
import android.os.Build
import android.util.Log
import androidx.annotation.RequiresApi
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import java.util.*

object Extensions {
    @SuppressLint("SimpleDateFormat")
    fun String.castToDate(): Date {
        return SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSS").parse(this.replace("Z", ""))
    }

    fun Date.formattedDate(): String? {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            return this.convertToLocalDateViaInstant()
                ?.format(DateTimeFormatter.ofPattern("MM-dd-yyyy"))
        }
        return "${this.month}-${this.day}-${this.year}"
    }

    fun Date.getDayOrdinal(): String? {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val day = this.convertToLocalDateViaInstant()?.format(DateTimeFormatter.ofPattern("dd"))
            return try {
                (day?.toInt())?.ordinalOf()
            } catch (e: Exception) {
                Log.e("ERROR CAST ORDINAL", "${e.message}")
                null
            }
        }
        return this.day.ordinalOf()
    }

    fun Date.getCustomYear(): String? {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            return try {
                this.convertToLocalDateViaInstant()?.format(DateTimeFormatter.ofPattern("yyyy"))
            } catch (e: Exception) {
                Log.e("ERROR CAST ORDINAL", "${e.message}")
                null
            }
        }
        return "${this.year}"
    }

    fun Date.getCustomMonth(): String? {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            return try {
                this.convertToLocalDateViaInstant()?.format(DateTimeFormatter.ofPattern("MMM"))
            } catch (e: Exception) {
                Log.e("ERROR CAST ORDINAL", "${e.message}")
                ""
            }
        }
        return ""
    }

    fun Int.ordinalOf() = "$this" + if (this % 100 in 11..13) "th" else when (this % 10) {
        1 -> "st"
        2 -> "nd"
        3 -> "rd"
        else -> "th"
    }

    @RequiresApi(Build.VERSION_CODES.O)
    fun Date.convertToLocalDateViaInstant(): LocalDate? {
        return this.toInstant()
            .atZone(ZoneId.systemDefault())
            .toLocalDate()
    }

}