package com.keylimetie.swinggame.utils

import android.content.Context
import android.util.Log
import com.keylimetie.swinggame.ActionList
import com.keylimetie.swinggame.BaseActivity
import com.keylimetie.swinggame.PlayerList


enum class PreferenceTypeKey { PLAYERS, ACTIONS, PREFS_DATA }
object Preferences {


    fun saveDataPreference(
        context: BaseActivity,
        keyPreference: PreferenceTypeKey,
        jsonData: String
    ) {
        context.sharedPreferences = context.getSharedPreferences(
            PreferenceTypeKey.PREFS_DATA.name,
            0
        )
        val editor = context.sharedPreferences?.edit()
        editor?.putString(keyPreference.name, jsonData)
        editor?.apply()
    }

    /**
     * GET CURRENT TOKEN
     * */
    fun getActionListStored(context: BaseActivity): ActionList? {
        context.sharedPreferences = context.getSharedPreferences(
            PreferenceTypeKey.PREFS_DATA.name,
            Context.MODE_PRIVATE
        )
        val dataJsonAsString =
            context.sharedPreferences!!.getString(PreferenceTypeKey.ACTIONS.name, "")

        return try {
            val user: ActionList? =
                dataJsonAsString?.let { ActionList.fromJson(it) }

            user

        } catch (e: Exception) {
            Log.e("PREFSS", "${e.message}")

            null
        }
    }

    fun getPlayersStored(context: BaseActivity): PlayerList? {
        context.sharedPreferences = context.getSharedPreferences(
            PreferenceTypeKey.PREFS_DATA.name,
            Context.MODE_PRIVATE
        )
        val dataJsonAsString =
            context.sharedPreferences!!.getString(PreferenceTypeKey.PLAYERS.name, "")

        return try {
            val user: PlayerList? =
                dataJsonAsString?.let { PlayerList.fromJson(it) }

            user

        } catch (e: Exception) {
            Log.e("PREFSS", "${e.message}")

            null
        }
    }


    /**
     * REMOVE PREFERENCE DATA
     */
    fun removeReferenceData(context: BaseActivity, keyPreference: PreferenceTypeKey) {
        val editor = context.sharedPreferences!!.edit()
        editor.remove(keyPreference.name)
        editor.clear()
        editor.commit()
        editor.apply()
    }

}

//fun BaseActivity.uploadCurrentUserImageProfile(imageView: ImageView) {
//    storage = FirebaseStorage.getInstance(
//        FirebaseApp.initializeApp(this)!!,
//        BaseStatics.FIREBASE_STORAGE_URL
//    )//"")
//    storageRef = storage?.reference
//
//    val userNameAsNameFolder =
//        if (Preferences.getCurrentUser(this)?.email?.isNullOrEmpty() ?: return)
//            ""
//        else Preferences.getCurrentUser(this)?.email?.split("@")?.get(0)?.replace(".", "$")
//    val userProfileReference =
//        storageRef?.child("images_user_eds/$userNameAsNameFolder/$userNameAsNameFolder.jpg")
//    uploadImage(imageView, userProfileReference ?: return)
//}
