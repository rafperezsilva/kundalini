package com.keylimetie.swinggame

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.databind.PropertyNamingStrategy
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue

val mapper = jacksonObjectMapper().apply {
    propertyNamingStrategy = PropertyNamingStrategy.LOWER_CAMEL_CASE
    setSerializationInclusion(JsonInclude.Include.NON_NULL)
}

data class PlayerList(
    var players: ArrayList<Player>? = null
) {
    fun toJson() = mapper.writeValueAsString(this)

    companion object {
        fun fromJson(json: String) = mapper.readValue<PlayerList>(json)
    }
}

data class ActionList(
    var actions: ArrayList<Action>? = null
) {
    fun toJson() = mapper.writeValueAsString(this)

    companion object {
        fun fromJson(json: String) = mapper.readValue<ActionList>(json)
    }
}

data class Player(
    var name: String? = null,
    var gender: Gender? = Gender.FEMALE,
    var orientation: SexualOrientation? = SexualOrientation.BISEXUAL,
) {
    fun toJson() = mapper.writeValueAsString(this)

    companion object {
        fun fromJson(json: String) = mapper.readValue<Player>(json)
    }
}

data class Action(
    var action: String? = null,
    var canDoIt: Gender? = Gender.MALE,
    var onWho: Gender? = Gender.MALE,
    var timeStandar: TimeStandard? = TimeStandard.MINUTES,
    var timeCount: Int? = null,

    ) {
    fun toJson() = mapper.writeValueAsString(this)

    companion object {
        fun fromJson(json: String) = mapper.readValue<Player>(json)
    }
}


enum class TimeStandard { SECONDS, MINUTES }
enum class Gender { MALE, FEMALE, BOTH }
enum class SexualOrientation { HETEROSEXUAL, BISEXUAL }